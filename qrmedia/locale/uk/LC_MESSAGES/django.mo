��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  �  �  !   2  %   T  "   z  (   �      �     �     �  &        ;  ,   [     �  2   �     �  &   �       I   *  #   t  &   �  *   �     �     �  !               	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: uk
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 (показати/сховати) Розширені параметри Стаття у Вікіпедії Курси у Віківерситеті Дані на Вікіданих Дані з Reasonator Розроблено: Файли на Вікісховищі Репозиторій Framagit Путівник на Вікімандрах Домашня сторінка Посібники у Вікіпідручнику Новий QRCode Новини у Вікіновинах Тег проєкту Генератор QR-кодів до сторінок Вікімедіа Місце показу QR-коду Цитати у Вікіцитатах Тексти на Вікіджерелах The QRMedia Portal Необхідний Qid. Елемент Вікіданих 