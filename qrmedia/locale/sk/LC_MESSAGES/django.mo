��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  M  �     �          &     =     R     d     w     �     �     �     �     �     �            ,   ,     Y     o     �     �     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: sk
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ( (n >= 2 && n <= 4) ? 1 : 2 );
 (zobraziť/schovať) Pokročilé parametre Položka na Wikipédii Kurzy na Wikiversity Dáta na Wikidata Dáta s Reasonator Vyvíjané cez Súbory na Wikimedia Commons Úložisko Framagit Sprievodca na Wikivoyage Domovská stránka Manuály na Wikibooks Nový QR kód Novinky na Wikinews Značka projetku Generátor QR kódov pre stránky Wikimédia Lokalizácia QR kódu Citáty na Wikiquote Texty na Wikisource QRMedia Portál Povinný údaj: Qid Wikidata položka 