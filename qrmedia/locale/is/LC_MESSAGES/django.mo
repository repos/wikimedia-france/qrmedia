��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �     �     	     #     5     K     W     s     �     �     �     �     �     �  #         %     F     ]     r      �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: is
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (sýna/fela) Ítarleg viðföng Grein á Wikipedia Námskeið á Wikiversity Gögn á Wikidata Gögn með Reasonator Skrifað af Skrár á Wikimedia Commons Framagit-hugbúnaðarsafn Leiðarvísir á Wikivoyage Heimasíða Handbækur á Wikibooks Nýr QR-kóði Fréttir á Wikinews Merki verkefnis QR-kóðari fyrir Wikimedia-síður Staðsetning birtingar QR-kóða Tilvitnun á Wikiquote Textar á Wikisource QRMedia gáttin Qid-auðkennið er nauðsynlegt. Wikidata-atriði 