��          �      \      �     �     �     �               .     ;     V     j     s  
   �     �     �     �     �     �     �            w  '     �     �     �     �     �               )     >     P     i     y     �     �     �     �     �     �              
                             	                                                             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Developed by Files on Wikimedia Commons Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: lt
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 (rodyti/slėpti) Išplėstiniai parametrai Straipsnis Vikipedijoje Kursai Vikiversitete Duomenys Vikiduomenyse Sukūrė Failai Vikitekoje Gidas Vikikelionėse Pradinis puslapis Instrukcijos Vikiknygose Naujas QR kodas Naujienos Vikinaujienose Projekto žyma QR kodo rodymo vieta Citatos Vikicitatose Tekstai Vikišaltiniuose QRMedia portalas Reikalingas Qid. Vikiduomenų elementas 