��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �     �          "     ;     O     ]  "   }     �  
   �     �     �     �     �  +        ?     \  #   t     �     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: io
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (montrez/celez) Specala parametri Artiklo de Wikipedio Kursi de Wikiversity Datumaro de ''Wikidata'' Dati kun Reasonator Developita da Arkivi de ''Wikimedia Commons'' Depozeyo (''repository'') Fragamit Voyajo-guidili de Wikivoyage Chefpagino Manuali de Wikibooks Nova QR-kodexo Novaji de Wikinews Etiketo dil projeto Kreilo di QR-kodexo por pagini de Wikimedia lokizo ube videsos QR-kodexo Citaji de ''Wikiquote'' Texti de Wikifonto (''Wikisource'') Portalo QRMedia ID de la feldo esas necesa. Arkivo de Wikidata 