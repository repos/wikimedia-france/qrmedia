��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �     
  %   &  %   L     r     �  !   �     �     �     �  !        &     7     W  .   k     �  %   �  !   �       !        7            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: he
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (הצגה/הסתרה) פרמטרים מתקדמים ערך בוויקיפדיה קורסים בוויקיברסיטה נתונים בוויקינתונים נתונים עם Reasonator פותח על ידי קבצים בוויקישיתוף מאגר Framagit מדריך בוויקימסע דף הבית מדריכים בוויקיספר קוד QR חדש כתבה בוויקיחדשות תגית המיזם מחולל קוד QR לדפי ויקימדיה מיקום תצוגת קוד QR ציטוטים בוויקיציטוט טקסטים בוויקיטקסט פורטל QRMedia מזהה הנתונים נדרש. פריט ויקינתונים 