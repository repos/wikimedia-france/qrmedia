��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  !  �     �     �     �     	           1     E     S     q     �     �     �     �     �     �  )   �          5     I     `     p     }            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: tr
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=1; plural=0;
 (Göster/gizle) Gelişmiş parametreler Vikipedi'de madde Vikiversite'de kurslar Vikiveri'de veri Rezonatör ile Veri Geliştirien: Wikimedia Commons'ta dosyalar Framagit deposu Vikigezgin'de kılavuz Anasayfa Vikikitap'ta el kitapları Yeni QR Kodu Vikihaber'de haberler Proje etiketi Wikimedia sayfalarına QR Kod oluşturucu QR kodu görüntüleme konumu Vikisöz'de sözler Vikikaynak'ta metinler QRMedia Portali Qid gerekli. Vikiveri ögesi 