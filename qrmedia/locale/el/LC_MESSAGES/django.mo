��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �  #   �  )   �  (   $  #   M      q  %   �     �  %   �     �          9  %   S     y  &   �     �  F   �  4     '   D      l     �     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: el
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (εμφάνιση/απόκρυψη) Προηγμένες παράμετροι Άρθρο στη Βικιπαίδεια Μαθήματα στο Wikiversity Δεδομένα στο Wikidata Δεδομένα με το Reasonator Αναπτύχθηκε από Αρχεία στα Wikimedia Commons Αποθετήριο Framagit Οδηγός στο Wikivoyage Αρχική σελίδα Εγχειρίδια στο Wikibooks Νέο QRCode Ειδήσεις στα Βικινέα Ετικέτα έργου Γεννήτρια κωδικών QR προς σελίδες Wikimedia Τοποθεσία εμφάνισης του QR code Αποφθέγματα στο Wikiquote Κείμενα στο Wikisource Η πύλη QRMedia Το Qid απαιτείται. αντικείμενο Wikidata 