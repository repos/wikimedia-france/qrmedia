��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �  !   �  %   �  7     L   V  7   �  $   �  (      D   )  7   n  @   �     �  =        E  1   Y  %   �  v   �  A   (  .   j  1   �  )   �  /   �  .   %	            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: bn
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (দেখান/লুকান) উন্নত পরামিতি উইকিপিডিয়ার নিবন্ধ উইকিবিশ্ববিদ্যালয়ের কোর্স উইকিউপাত্তের উপাত্ত Reasonator সহ উপাত্ত উন্নয়ন করেছেন উইকিমিডিয়া কমন্সের ফাইল ফ্রেমজিট সংগ্রহস্থল উইকিভ্রমণের নির্দেশিকা প্রধান পাতা উইকিবইয়ের ম্যানুয়াল নতুন QRCode উইকিসংবাদের সংবাদ প্রকল্প ট্যাগ উইকিমিডিয়ার পৃষ্ঠাগুলিতে কিউআর কোড উৎপাদক QR কোড প্রদর্শনের অবস্থান উইকিউক্তির উক্তি উইকিসংকলনের পাঠ্য QRMedia প্রবেশদ্বার এই Qid বাধ্যতামূলক। উইকিউপাত্ত আইটেম 