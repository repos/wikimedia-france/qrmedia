��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  �  �  #   2  )   V  "   �  (   �  $   �     �       $   #     H  ,   d  !   �  2   �     �  *   �     #  @   =  ;   ~  *   �      �            #   3            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: ru
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 (показать/спрятать) Расширенные параметры Статья в Википедии Курсы в Викиверситете Данные в Викиданных Данные с Reasonator Разработано Файлы на Викискладе Хранилище Framagit Путеводитель в Викигиде Домашняя страница Руководства в Викиучебнике Новый QRCode Новости в Викиновостях Метка проекта Генератор QR-кода на страницах Wikimedia Расположение отображения QR-кода Цитаты в Викицитатнике Тексты в Викитеке Портал QRMedia Требуется Qid. Элемент Викиданных 