��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  !  �     �  #   �        +   ,     X     w     �  $   �     �      �       &        @     Q     j  D   �     �  .   �          3  !   F     h            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: fa
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=1; plural=0;
 (نمایش/پنهان) پارامترهای پیشرفته مقالهٔ ویکی‌پدیا دوره‌های ویکی‌دانشگاه دادهٔ ویکی‌داده Data with Reasonator توسعه‌دهنده: پروندهٔ ویکی‌انبار مخزن فراماگیت راهنمای ویکی‌سفر صفحهٔ اصلی راهنماهای ویکی‌کتاب کد QR جدید خبر ویکی‌خبر برچسب پروژه تولیدکنندهٔ کد QR صفحه‌های ویکی‌مدیا کد QR نمایش موقعیت گفتاوردهای ویکی‌گفتاورد متون ویکی‌نبشته درگاه QRMedia شناسهٔ Q ضروری است. آیتم ویکی‌داده 