��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �     �                2     F     R     n     |  
   �     �  
   �     �     �  $   �          !     7     N     _     q            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-04 11:16:22+0000
Language: nb
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (vis/skjul) Avanserte parametre Artikkel på Wikipedia Kurs i Wikiversity Data på Wikidata Data med Reasonator Utviklet av Filer på Wikimedia Commons Framagit-repo Gaid på Wikivoyage Hjemmeside Håndbøker på Wikibøker Ny QR-kode Nyheter på Wikinytt Prosjekttagg QR-kodegenerator for Wikimedia-sider Visningssted for QR-kode Sitater på Wikiquote Tekster på Wikikilden QRMedia-portalen Q-ID er påkrevd. Wikidata-element 