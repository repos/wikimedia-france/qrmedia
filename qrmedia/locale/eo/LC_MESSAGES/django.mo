��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �     �          +     C     Z     h     �     �  	   �     �     �     �     �  %        4     N     f     ~     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: eo
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (montri/kaŝi) Altnivelaj parametroj Artikolo ĉe Vikipedio Kursoj ĉe Vikiversitato Datumoj ĉe Vikidatumoj Datumoj per Reasonator Programita de Dosieroj ĉe Vikimedia Komunejo Framagit-deponejo Gvidiloj ĉe Vikivojaĝo Ĉefpaĝo Manlibroj ĉe Vikilibroj Nova QR-kodo Novaĵoj ĉe Vikinovaĵoj Projekta etikedo QR-kodogenerilo por vikimdeiaj paĝoj Montroloko por la QR-kodo Citaĵoj ĉe Vikicitaro Tekstoj ĉe Vikifontaro La QRMedia-portalo La Q-identigilo estas deviga. Vikidatumoj-ero 