��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  }  �     +     >     Y     m     �     �     �     �     �     �                =     M     b  1   r  %   �     �     �     �  ,        3            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Last-Translator: Emma Vadillo
Language-Team: 
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
X-POT-Import-Date: 2021-05-26 12:33:35+0000
 (mostrar/esconder) Configurações avançadas Artigo na Wikipedia Cursos na Wikiversity Dados na Wikidata Dados com Reasonator Desenvolvido pela Ficheiros na Wikimedia Commons Repositório Framagit Guia de viagem na Wikivoyage Página principal Livro didático na Wikibooks Novo código QR Notícia na Wikinews Tag de projecto Gerador de códigos QR para páginas da Wikimedia Local de apresentação do código QR Citações na Wikiquote Textos na Wikisource Portal QRMedia Q identificador d'iten Wikidata necessitado. Itens wikidata 