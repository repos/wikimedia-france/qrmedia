��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �    �  !   �  %   �  $     .   3  .   b     �     �  &   �     �  6   
     A  ,   _     �  &   �     �  I   �  #   ,  &   P  (   w     �     �  '   �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item PO-Revision-Date: 2023-04-27 11:17:59+0000
X-POT-Import-Date: 2021-05-26 12:33:35+0000
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: be-tarask
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (паказаць/схаваць) Пашыраныя парамэтры Артыкул у Вікіпэдыі Курсы ў Вікіўнівэрсытэце Зьвесткі ў Вікізьвестках Зьвесткі з Reasonator Распрацавана Файлы ў Вікісховішчы Сховішча Framagit Дапаможнікі ў Віківандроўках Хатняя старонка Падручнікі ў Вікікнігах Новы QR-код Навіны ў Вікінавінах Цэтлік праекту Генэратар QR-кодаў да старонак Вікімэдыі Месца вываду QR-коду Цытаты ў Вікіцытатах Тэксты ў Вікікрыніцах Партал QRMedia Патрэбны Qid. Элемэнт Вікізьвестак 