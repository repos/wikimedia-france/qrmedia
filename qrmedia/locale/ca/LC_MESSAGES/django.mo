��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  }  �     +     :     P     i     �     �     �     �     �     �     �          %     1     L  1   b     �     �     �     �  *   �                  	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Last-Translator: Emma Vadillo
Language-Team: 
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
X-POT-Import-Date: 2021-05-26 12:33:35+0000
 (mostra/amaga) Paràmetres avançats Article a la Viquipèdia Cursos a Viquiversitat Dades a Wikidata Dades amb Reasonator Desenvolupat per Fitxers a Wikimedia Commons Dipòsit al Framagit Guia a «Wikivoyage» Pàgina d'inici Manuals a Viquillibres Codi QR nou Notícies a Viquinotícies Etiqueta del projecte Generador de codis QR per a pàgines de Wikimedia Lloc de visualització Paremiologia a Viquidites Publicacions a Viquitexts El portal del QRMedia L'identificador de Wikdiata és necessari. Element de Wikidata 