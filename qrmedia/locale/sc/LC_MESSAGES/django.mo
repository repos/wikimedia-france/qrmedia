��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �     �          '     9     N     ]     }     �     �     �     �     �     �  4     *   =     h     |     �  4   �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: sc
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (mustra/cua) Paràmetros avantzados Artìculu in Wikipedia Cursos in Wikiversity Datos in Wikidata Datos cun Reasonator Isvilupadu dae Documentos in Wikimedia Commons Depòsitu in Framagit Ghia in Wikivoyage Pàgina printzipale Manuales in Wikibooks Còdighe QR nou Noas in Wikinews Eticheta de su progetu Generadore de còdighes QR pro pàginas de Wikimedia Tretu de visualizatzione de su còdighe QR Tzitas in Wikiquote Testos in Wikisource Su portale de su QRMedia S'identificadore de Wikidata (Qid) est netzessàriu. Elementu de Wikidata 