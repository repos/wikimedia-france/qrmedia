��          �      l      �     �     �               -     >     K     f     z     �     �  
   �     �     �  $   �     �          &     :     M  )  [  #   �  -   �      �      �  "     (   <  -   e  !   �  $   �     �  ,   �       "   6     Y  S   m  4   �     �  "     '   4     \                                      
                       	                                     (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: ckb
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (پیشاندان/شاردنەوە) پارامەترە پێشکەوتووەکان وتار لە ویکیپیدیا کۆرس لە ویکیزانکۆ دراوە لە ویکیدراوە پەرەی پێدراوە لەلایەن پەڕگە لە ویکیمیدیا کۆمنز سەرچاوەی فڕەیمگیت ڕێنوێنی لە ویکیگەشت دەستپێک ڕێنوێنینامە لە ویکیکتێب کۆدی کیو ئاڕی نوێ ھەواڵ لە ویکیھەواڵ تاگی پڕۆژە دروستکەری کۆدی کیو ئاڕ بۆ پەڕەکانی ویکیمیدیا. شوێنی پیشاندانی کۆدی کیو ئاڕ وتە لە ویکیوتە دەق لە ویکیسەرچاوە دەروازەی کیوئاڕمیدیا بەندی ویکیدراوە 