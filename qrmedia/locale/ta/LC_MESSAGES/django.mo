��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �  :   �  F   0  k   w  7   �  '     @   C  r   �  $   �  V     -   s  U   �     �  F     .   U  �   �  ;   		  L   E	  M   �	     �	     �	  =   
            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: ta
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (காண்பி/மறை) மேம்பட்ட அளவுருக்கள் விக்கிபீடியாவில் கட்டுரை விக்கிப்பல்கலைக் கழகத்தின் படிப்புகள் விக்கித்தரவில் தரவு Reasonator-வுடன் தரவு மூலம் உருவாக்கப்பட்டது விக்கிமீடியா பொதுவகத்தில் உள்ள கோப்புகள் Framagit களஞ்சியம் விக்கிப்பயணம் பற்றிய வழிகாட்டி முகப்புப்பக்கம் விக்கிபுத்தகங்களில் கையேடுகள் புதிய QRCode விக்கிசெய்திகளில் செய்தி திட்ட குறிச்சொல் விக்கிமீடியா பக்கங்களுக்கு QR குறியீடு உருவாக்கி QR குறியீடு காட்சி இடம் விக்கிமேற்கோள் மேற்கோள்கள் விக்கிமூலத்தில் உள்ள உரைகள் QRMedia புறையம் Qid தேவை. விக்கித்தரவு உருப்படி 