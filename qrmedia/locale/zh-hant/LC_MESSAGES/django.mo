��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �    �     �     �     �     �          2  	   N  !   X     z  !   �     �     �  
   �     �     �  %   	     /     B     ^     z     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item PO-Revision-Date: 2023-04-27 11:18:00+0000
X-POT-Import-Date: 2021-05-26 12:33:35+0000
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh-Hant
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=1; plural=0;
 （顯示/隱藏） 進階參數 在維基百科上的條目 在維基學院上的課程 在維基數據上的數據 使用 Reasonator 的資料 開發由 在維基共享資源上的檔案 Framagit 存儲庫 在維基導遊上的旅行指南 首頁 在維基教科書上的手冊 新 QR 碼 在維基新聞上的新聞 專案標籤 維基媒體頁面的 QR 碼產生器 QR 碼顯示位置 在維基語錄上的語錄 在維基文庫上的文本 QRMedia 入口 Qid 是必需的。 維基數據項目 