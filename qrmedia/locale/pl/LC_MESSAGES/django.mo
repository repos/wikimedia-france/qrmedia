��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  r  �           /     F     [     o     �     �     �     �     �     �     �          !     8  '   E     m     �     �     �     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: pl
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 (pokaż/ukryj) Zaawansowane parametry Artykuł w Wikipedii Kursy w Wikiversity Dane w Wikidanych Dane z Reasonator Stworzony przez Pliki w Wikimedia Commons Repozytorium Framagit Przewodniki w Wikivoyage Strona główna Podręczniki w Wikibooks Nowy kod QR Wiadomości w Wikinews Tag projektu Generator kodów QR dla stron Wikimedii Wyświetl lokalizację kodu QR Cytaty w Wikicytatach Źródła w Wikiźródłach Portal QRMedia Qid jest wymagany. Element Wikidanych 