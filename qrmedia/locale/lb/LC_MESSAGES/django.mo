��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �                ,     C     ]     m     �     �  
   �     �     �     �  
   �  (        0     L     a     v     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: lb
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (weisen/verstoppen) Erweidert Parameteren Artikel op Wikipedia Coursen op Wikiversity Donnéeën op Wikidata Donnéeën mam Reasonator Entwéckelt vum Fichieren op Wikimedia Commons Framagit-Repository Guide op Wikivoyage Haaptsäit Handbicher op Wikibooks Neie QR-Code Neiegkeeten op Wikinews Projet-Tag QR-Code-Generateur fir Wikimedia-Säiten Plaz vum Uweise vum QR-Code Zitater op Wikiquote Texter op Wikisource De QR-Media-Portal D'Q-ID ass obligatoresch. Wikidata-Element 