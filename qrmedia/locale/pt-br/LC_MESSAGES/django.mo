��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �    �     �     �     �                *     ?     P     n     �     �     �     �     �     �  1   �  !   /     Q     i     ~  (   �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item PO-Revision-Date: 2023-04-27 11:18:00+0000
X-POT-Import-Date: 2021-05-26 12:33:35+0000
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt-BR
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n > 1);
 (mostrar/esconder) Parâmetros avançadas Artigo na Wikipédia Cursos no Wikiversidade Dados no Wikidata Dados com Reasonator Desenvolvido por Arquivos no Wikimedia Commons Repositório Framagit Guia no Wikivoyage Página principal Manuais no Wikilivros Novo código QR Notícia no Wikinotícias Etiqueta de projeto Gerador de códigos QR para páginas da Wikimedia Local de exibição do código QR Citações no Wikiquote Textos na Wikisource Portal QRMedia O identificador Wikidata é necessário. Item do Wikidata 