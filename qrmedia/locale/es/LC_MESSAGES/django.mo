��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  }  �     +     =     S     j     �     �     �     �     �     �               -     >     [  4   q  %   �     �     �     �     	                  	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Last-Translator: Emma Vadillo
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
X-POT-Import-Date: 2021-05-26 12:33:35+0000
 (mostrar/ocultar) Parámetros avanzados Artículo en Wikipedia Cursos en Wikiversidad Datos en Wikidata Datos con Reasonator Desarrollado por Archivos en Wikimedia Commons Repositorio en Framagit Guías en Wikivoyage Página principal Manuales en Wikilibros Código QR nuevo Actualidades en Wikinoticias Etiqueta del proyecto Generador de códigos QR hacia páginas de Wikimedia Lugar de visualización de código QR Citas en Wikiquote Textos en Wikisource El portal de QRMedia El Qid es obligatorio. Elemento de Wikidata 