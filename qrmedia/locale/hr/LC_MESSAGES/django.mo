��          |      �             !     -     B     Y     j     w     �     �     �     �     �  �  �     q     �     �     �  	   �  4   �          (     =     M     b                                           
         	       (show/hide) Article on Wikipedia Courses on Wikiversity Data on Wikidata Developed by Files on Wikimedia Commons Manuals on Wikibooks News on Wikinews Project tag Quotes on Wikiquote Texts on Wikisource Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: hr
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 (prikaži/skrij) Članak na Wikipediji Tečajevi na Wikiučilištu Podatci na Wikipodatcima Razvio/la Datoteke na Wikimedijinom Zajedničkom poslužitelju Priručnici na Wikiknjigama Novosti na Wijestima Oznaka projekta Citati na Wikicitatu Tekstovi na Wikizvoru 