��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  =  �     �  #     (   +  6   T  2   �  (   �     �  ,   �     )  (   E     n  0   �     �  "   �     �  R     /   e  $   �  (   �     �     �  0               	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: mk
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 (прикажи/скриј) Напредни параметри Статија на Википедија Курсеви на Викиуниверзитетот Податоци на Википодатоците Податоци со Резонатор Разработил Податотеки на Ризницата Складиште Framagit Водич на Википатување Домашна страница Прирачници на Викикнигите Нов QR-код Вести на Викивести Ознака за проект Создавач на QR-кодови за Викимедиини страници Место за приказ на QR-кодот Цитати на Викицитат Текстови на Викиизвор Портал QRMedia Се бара Qid. Предмет на Википодатоците 