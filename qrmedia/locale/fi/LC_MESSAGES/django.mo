��          �      L      �     �     �     �     �               +     F     Z     n     w  
   �     �     �     �     �     �     �  (  �     &     8     N     e     {     �     �     �     �     �     �               '     :     S     k     |                                       
                       	                                      (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag Quotes on Wikiquote Texts on Wikisource The QRMedia Portal Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:17:59+0000
Language: fi
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (näytä/piilota) Edistyneet parametrit Artikkeli Wikipediassa Kurssit Wikiopistossa Tiedot Wikidatassa Kehittänyt Tiedostot Wikimedia Commonsissa Framagit-koodivaranto Opas Wikimatkoissa Kotisivu Kirjat Wikikirjastossa Uusi QR-koodi Uutinen Wikiuutisissa Projektin tunniste Sitaatit Wikisitaateissa Tekstit Wikiaineistossa QRMedia-portaali Wikidata-kohde 