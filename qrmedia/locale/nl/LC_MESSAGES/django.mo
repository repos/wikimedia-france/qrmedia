��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  (  �     �     �                .     C     [     k     �     �     �     �     �     �     �  (   	     2     J     _     u     �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: nl
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=2; plural=(n != 1);
 (tonen/verbergen) Geavanceerde parameters Artikel op Wikipedia Cursussen op Wikiversity Gegevens op Wikidata Gegevens met Reasonator Ontwikkeld door Bestanden op Wikimedia Commons Framagit-repository Gids op Wikivoyage Startpagina Handleidingen op Wikibooks Nieuwe QR-code Nieuws op Wikinieuws Projectlabel QR-codegenerator voor Wikimedia-pagina's Weergavelocatie QR-code Citaten op Wikiquote Teksten op Wikisource Het QRMedia-portaal De Qid is verplicht. Wikidata-item 