��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �    �     �     �     �     �            	   .     8     W     f          �     �     �     �  $   �     �          %     8     F     Y            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item PO-Revision-Date: 2023-04-27 11:18:00+0000
X-POT-Import-Date: 2021-05-26 12:33:35+0000
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh-Hans
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=1; plural=0;
 （显示/隐藏） 高级参数 维基百科条目 维基学院课程 维基数据上的数据 Reasonator数据 开发： 维基共享资源上的文件 Framagit仓库 维基导游上的指南 首页 维基教科书上的手册 新二维码 Wikinews上的新闻 项目标签 Wikimedia页面的二维码生成器 二维码显示位置 维基语录上的语录 维基文库文献 QRMedia门户 必须填写Qid。 维基数据项 