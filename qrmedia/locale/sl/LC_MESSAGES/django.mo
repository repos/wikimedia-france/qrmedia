��          �      �           	          )     >     U     f     {     �     �     �     �     �  
   �     �       $        6     O     c     w     �     �  p  �          /     B     W     k     �     �     �     �     �     �     �                4  %   D     j     �     �     �  .   �     �            	                                                                               
             (show/hide) Advanced parameters Article on Wikipedia Courses on Wikiversity Data on Wikidata Data with Reasonator Developed by Files on Wikimedia Commons Framagit repository Guide on Wikivoyage Homepage Manuals on Wikibooks New QRCode News on Wikinews Project tag QR Code generator to Wikimedia pages QR code display location Quotes on Wikiquote Texts on Wikisource The QRMedia Portal The Qid is required. Wikidata item Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-27 11:18:00+0000
Language: sl
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2021-05-26 12:33:35+0000
X-Generator: MediaWiki 1.41.0-alpha; Translate 2023-04-26
Plural-Forms: nplurals=4; plural=(n%100 == 1) ? 0 : ( (n%100 == 2) ? 1 : ( (n%100 == 3 || n%100 == 4) ? 2 : 3 ) );
 (prikaži/skrij) Napredni parametri Članek v Wikipediji Tečaji v Wikiverzi Podatki v Wikipodatkih Podatki z Reasonatorjem Razvijalec: Datoteke v Wikimedijini zbirki Shramba Framagit Vodnik v Wikipotovanju Glavna stran Priročnik v Wikiknjigah Nova koda QR Novice v Wikipediji Oznaka projekta Generator kod QR na strani Wikipedije Mesto prikaza kode QR Navedki v Wikiviru Besedila v Wikiviru Portal QRMedia Potreben je identifikator v Wikipodatkih (Qid) Predmet v Wikipodatkih 