from django.test import TestCase

# Model tests
from .models import *


class ArticleTest(TestCase):
    def setUp(self):
        pass

    def test_create_article(self):
        # When an article is called, there is a result
        london = Article("Q84", lang="en")
        self.assertEqual(london.qid, "Q84")
        self.assertEqual(london.lang, "en")

        london = Article("Q84", lang="en")
        london.get_live_wd_data()
        self.assertEqual(london.wd_label, "London")

        london.get_wp_summary()
        self.assertIn("London", london.wp_summary)

    def test_call_article_language(self):
        # The result is language dependant
        londres = Article("Q84", lang="fr")
        londres.get_live_wd_data()
        self.assertEqual(londres.wd_label, "Londres")


class OtherFunctionsTest(TestCase):
    def test_get_works(self):
        # Test with Louise Michel
        works = get_works("Q216092", lang="fr", limit=3)
        testwork = {
            "title": "La Commune",
            "sitelink": "https://fr.wikisource.org/wiki/La_Commune_(Michel)",
        }
        self.assertIn(testwork, works)

    def test_get_commons_files(self):
        birbs = get_commons_files("Ravens in art", number=3, thumb_size=123)
        self.assertEqual(len(birbs["files"]), 3)
        first_file = birbs["files"][0]
        self.assertIn("/123px-", first_file["thumb"])

    def test_sanitize_file_name(self):
        sanitized = sanitize_file_name("File:Test name.jpg")
        self.assertEqual(sanitized, "Test_name.jpg")

    def test_commons_file_url(self):
        test_url = commons_file_url("test_file.png")
        assert_url = "https://upload.wikimedia.org/wikipedia/commons/c/c8/test_file.png"
        self.assertEqual(test_url, assert_url)

        test_url = commons_file_url("test_file.png", 80)
        assert_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/test_file.png/80px-test_file.png"
        self.assertEqual(test_url, assert_url)

    def test_extract_year(self):
        y = extract_year({"time": "2001-01-01T00:00:00Z"})
        self.assertEqual(y, "2001")
