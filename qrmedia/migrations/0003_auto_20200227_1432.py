# Generated by Django 3.0.3 on 2020-02-27 14:32

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ("qrmedia", "0002_panel"),
    ]

    operations = [
        migrations.CreateModel(
            name="Item",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("qid", models.CharField(max_length=20, unique=True)),
                ("label", models.CharField(max_length=200)),
                (
                    "created_date",
                    models.DateTimeField(default=django.utils.timezone.now),
                ),
            ],
        ),
        migrations.AlterField(
            model_name="panel",
            name="place",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="panel_place",
                to="qrmedia.Item",
            ),
        ),
        migrations.AlterField(
            model_name="panel",
            name="qid",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="panel_qid",
                to="qrmedia.Item",
            ),
        ),
    ]
