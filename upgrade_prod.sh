#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")"
git pull;
source venv/bin/activate;
pip install -r requirements.txt;
python3 manage.py collectstatic --no-input;
python3 manage.py migrate;
deactivate;
sudo service uwsgi restart && echo "service uwsgi restarted";
